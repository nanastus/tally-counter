import logger from './logger.mjs';

class Counter {
    constructor(initialValue = 0) {
        this.value = initialValue;
    }

    increase() {
        this.value++;
        //logger.info(`[COUNTER] increase ${this.value}`);
    }

    zero() {
        this.value = 0;
        //logger.info(`[COUNTER] zero ${this.value}`);
    }

    read() {
        //logger.info(`[COUNTER] read ${this.value}`);
        return this.value;
    }
    get count() {
        return this.value;
    }
}

export default Counter;
